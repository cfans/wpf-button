﻿using System.Windows;


namespace CF_WPF_Button
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            ImageWindow window = new ImageWindow();
            window.ShowDialog();
        }
    }
}
